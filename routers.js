const express = require('express');
const filesRouter = express.Router();
const {createFile, getAllFiles, getFile, deleteFile, modifyFileContent} = require('./filesService');
const {checkFileExtension, checkCreateFile, checkFileInFolder, checkCreateFilesDir} = require('./middleware');

filesRouter.post('/', checkCreateFilesDir, checkCreateFile, checkFileExtension, createFile);

filesRouter.get('/', checkCreateFilesDir, getAllFiles);

filesRouter.get('/:filename', checkCreateFilesDir, checkFileExtension, checkFileInFolder, getFile);

filesRouter.delete('/:filename', checkCreateFilesDir, checkFileExtension, checkFileInFolder, deleteFile);

filesRouter.put('/:filename', checkCreateFilesDir, checkFileExtension, checkFileInFolder, modifyFileContent);

module.exports = {
  filesRouter
}