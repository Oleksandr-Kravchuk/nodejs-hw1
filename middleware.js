const fs = require('fs').promises;

const checkCreateFilesDir = async (req, res, next) => {
  const rootDirContent = await fs.readdir(`${__dirname}`);
  const isFilesDir = rootDirContent.some( file => file === 'files');

  if (!isFilesDir) {
    await fs.mkdir(`${__dirname}/files`);
  }

  next();
};

const checkCreateFile = async (req, res, next) => {
  const {filename, content} = req.body;
  const files = await fs.readdir(`${__dirname}/files`);
  const isFileInDir = files.find(file => file === filename);

  if(isFileInDir) {
    return res.status(400).json({message: 'A file with this name already present in the folder'});
  }

  if (!req.body || !Object.keys(req.body).length) {
    return res.status(400).json({message: 'Request body is empty'});
  }

  if(!filename) {
    return res.status(400).json({message: `Please specify 'filename' parameter`});
  }

  if(!content) {
    return res.status(400).json({message: `Please specify 'content' parameter`});
  }

  next();
};

const checkFileExtension = async (req, res, next) => {
  const validExtension = /\S+\.(log|txt|gif|json|yaml|xml|js)$/gi;
  const fileName = req.params.filename || req.body.filename;
  const isValidExtension = validExtension.test(fileName);

  if (!isValidExtension) {
    return res.status(400).json({message: 'Wrong file extension'});
  }

  next();
};

const checkFileInFolder = async (req, res, next) => {
  const fileName = req.params.filename;
  const files = await fs.readdir(`${__dirname}/files`);
  const currentFile = files.find(file => file === fileName);

  if (!currentFile) {
    return res.status(400).json({ message: `No file with ${fileName} filename found` });
  }

  next();
};

const wrongRequestError = (req, res) => {
  res.status(404).json({ message: 'Not Found. Check your request' })
};

module.exports = {
  checkFileExtension,
  checkCreateFile,
  checkFileInFolder,
  checkCreateFilesDir,
  wrongRequestError
}