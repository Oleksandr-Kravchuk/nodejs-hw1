const express = require('express');
const morgan = require('morgan');
const {filesRouter} = require('./routers');
const {wrongRequestError} = require('./middleware'); 
const app = express();
const port = 8080;

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/files', filesRouter);
app.use(wrongRequestError);

app.listen(port, () => {
  console.log(`The server is listening on port ${port}!`);
});