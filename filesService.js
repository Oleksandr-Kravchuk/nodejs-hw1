const fs = require('fs').promises;
const path = require('path');

const createFile = async (req, res) => {
  const {filename, content} = req.body;
  
  try {
    await fs.writeFile(`./files/${filename}`, content);
    res.status(200).json({message: 'File created successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
  }
};

const getAllFiles = async (req, res) => {
  try {
    const files = await fs.readdir(`${__dirname}/files`);

    res.status(200).json({
      message: "Success",
      files
    });
  } catch (err) {
    res.status(500).json({message: 'Server error'});
  }
};

const getFile = async (req, res) => {
  try {
    const fileName = req.params.filename;
    const fileExtension = path.extname(fileName).slice(1);
    const uploadedDate = (await fs.stat(`${__dirname}/files`)).birthtime;
    const fileContent = await fs.readFile(`./files/${fileName}`, 'utf-8');

    res.status(200).json(
      {
        message: 'Success',
        filename: fileName,
        content: fileContent,
        extension: fileExtension,
        uploadedDate
      }
    );
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
};

const deleteFile = async (req, res) => {
  try {
    const fileName = req.params.filename;

    await fs.unlink(`./files/${fileName}`);
    res.status(200).json({ message: `File ${fileName} deleted successfully` });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
};

const modifyFileContent = async (req, res) => {
  try {
    const filename = req.params.filename;
    const {content} = req.body;

    if (!Object.keys(req.body).length) {
      return res.status(400).json({message: 'Request body is empty'});
    }

    if(!content) {
      return res.status(400).json({message: `Please specify 'content' parameter`});
    }

    await fs.writeFile(`./files/${filename}`, content);
    res.status(200).json({message: 'File updated successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
  }
};

module.exports = {
  createFile,
  getAllFiles,
  getFile,
  deleteFile,
  modifyFileContent
}